from django import forms
from django.contrib.auth import authenticate, get_user_model

User = get_user_model()


class UserLoginForm(forms.Form):
    """
    This form is subclassed from django form to validate user log in attempt.
    """
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError('This user does not exist')

            if not user.check_password(password):
                raise forms.ValidationError('Incorrect password')

            if not user.is_active:
                raise forms.ValidationError('This user is not active')

        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegisterForm(forms.ModelForm):
    """
    This form is subclassed from django model form to validate new user registration attempt.
    User model of django auth app is used here.
    """
    email = forms.EmailField(label='Email')
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'password2']

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        email_qs = User.objects.filter(email=email)

        if email_qs.exists():
            raise forms.ValidationError("This email is already registered")

        if password != password2:
            raise forms.ValidationError("Password did not match")

        return super(UserRegisterForm, self).clean(*args, **kwargs)




