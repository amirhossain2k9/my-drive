from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, get_user_model, login, logout
from accounts.forms import UserLoginForm, UserRegisterForm
from django.contrib.auth.decorators import login_required


def login_view(request):
    """
    User login request is processed by this view function.
    :param request:
    :return:
    """
    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect('/')

    context = {
        'form': form,
    }

    return render(request, 'accounts/login.html', context)


def register_view(request):
    """
    New user registration is processed here.
    :param request:
    :return: redirect to home page if successfully registered
    """

    # checked if logged in user tries to get the register page on same session
    if request.user.is_authenticated:
        return redirect('/')

    next = request.GET.get('next')
    form = UserRegisterForm(request.POST or None)

    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        if next:
            return redirect(next)
        return redirect('/')

    context = {
        'form': form,
    }

    return render(request, 'accounts/signup.html', context)


@login_required
def logout_view(request):
    """
    Log out a user from his/her logged in account.
    :param request:
    :return:
    """
    logout(request)
    return redirect('/')

