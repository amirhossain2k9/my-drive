from django.test import TestCase
from accounts.forms import UserLoginForm, UserRegisterForm
from django.contrib.auth.models import User


class UserRegisterFormTest(TestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.get(username='tester1')

    def test_new_user_registration(self):
        data = {
            'username': 'tester3',
            'email': 'tester3@test.com',
            'password': 'test1234',
            'password2': 'test1234'
        }


        form = UserRegisterForm(data=data)
        self.assertTrue(form.is_valid())

    def test_existing_usernam_email_registration(self):
        data = {
            'username': self.user.username,
            'email': 'tester3@test.com',
            'password': 'test1234',
            'password2': 'test1234'
        }

        form = UserRegisterForm(data=data)
        self.assertFalse(form.is_valid())

        data = {
            'username': 'tester3',
            'email': self.user.email,
            'password': 'test1234',
            'password2': 'test1234'
        }

        form = UserRegisterForm(data=data)
        self.assertFalse(form.is_valid())

    def test_empty_form_submission(self):
        data = {
            'username': '',
            'email': '',
            'password': '',
            'password2': ''
        }

        form = UserRegisterForm(data=data)
        self.assertFalse(form.is_valid())



    def test_field_missing(self):
        data1 = {
            'email': 'tester3@test.com',
            'password': 'test1234',
            'password2': 'test1234'
        }

        data2 = {
            'username': 'tester3',
            'password': 'test1234',
            'password2': 'test1234'
        }

        data3 = {
            'username': 'tester3',
            'email': 'tester3@test.com',
            'password2': 'test1234'
        }

        data4 = {
            'username': 'tester3',
            'email': 'tester3@test.com',
            'password': 'test1234',
        }


        form = UserRegisterForm(data=data1)
        self.assertFalse(form.is_valid())

        form = UserRegisterForm(data=data2)
        self.assertFalse(form.is_valid())

        form = UserRegisterForm(data=data3)
        self.assertFalse(form.is_valid())

        form = UserRegisterForm(data=data4)
        self.assertFalse(form.is_valid())

    def test_missmatch_password(self):
        data = {
            'username': 'tester3',
            'email': 'tester3@test.com',
            'password': 'test1234',
            'password2': 'randompass'
        }

        form = UserRegisterForm(data=data)
        self.assertFalse(form.is_valid())


class UserLoginFormTest(TestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.get(username='tester1')


    def test_valid_user_login(self):

        data = {
            'username': self.user.username,
            'password': 'test1234'
        }

        form = UserLoginForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_user_login(self):

        data = {
            'username': 'random',
            'password': 'randompass'
        }

        form = UserLoginForm(data=data)
        self.assertFalse(form.is_valid())

    def test_incorrect_password_login(self):

        data = {
            'username': self.user.username,
            'password': 'randompass'
        }

        form = UserLoginForm(data=data)
        self.assertFalse(form.is_valid())




