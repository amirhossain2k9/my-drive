from django.contrib.auth.models import AnonymousUser, User
from django.test import RequestFactory, TestCase, Client

from accounts.views import logout_view

class RegisterViewTest(TestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.get(username='tester1')
        cls.client =Client()
        cls.data = {
            'username': 'tester3',
            'email': 'tester3@test.com',
            'password': 'test1234',
            'password2': 'test1234'
        }

    def test_get_registration_page(self):
        response = self.client.get('/accounts/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_new_user(self):
        response = self.client.post('/accounts/register/', data=self.data)
        self.assertEqual(response.status_code, 302)

    def test_register_existing_user(self):
        data = {
            'username': self.user.username,
            'email': self.user.email,
            'password': 'test1234',
            'password2': 'test1234'
        }
        response = self.client.post('/accounts/register/', data=data)
        self.assertNotEqual(response.status_code, 302)

    def test_register_with_missmatch_password(self):
        data = {
            'username': 'tester3',
            'email': 'tester3@test.com',
            'password': 'test1234',
            'password2': 'randompass'
        }

        response = self.client.post('/accounts/register/', data=data)
        self.assertNotEqual(response.status_code, 302)


class LoginViewTest(TestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.get(username='tester1')
        cls.client = Client()
        cls.data = {
            'username': 'tester3',
            'password': 'test1234',
        }

    def test_getting_login_page(self):

        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)


    def test_login_valid_user(self):
        data = {
            'username': self.user.username,
            'password': 'test1234',
        }

        response = self.client.post('/accounts/login/', data=data)
        self.assertEqual(response.status_code, 302)

    def test_login_invalid_user(self):

        response = self.client.post('/accounts/login/', data=self.data)
        self.assertNotEqual(response.status_code, 302)


class LogoutViewTest(TestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.get(username='tester1')
        cls.factory = RequestFactory()
        cls.client = Client()
        cls.data = {
            'username': 'tester3',
            'password': 'test1234',
        }

    def test_login_valid_user(self):
        data = {
            'username': self.user.username,
            'password': 'test1234',
        }

        response = self.client.get('/accounts/login/')
        logged_in = self.client.login(**data)
        self.assertEqual(logged_in, True)

        logged_out = self.client.logout()
