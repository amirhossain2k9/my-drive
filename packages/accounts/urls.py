from django.urls import path, include, re_path
from accounts import views

app_name = "accounts"

urlpatterns = [
    path('login/', views.login_view, name='login'),
    path('register/', views.register_view, name='signup'),
    path('logout/', views.logout_view, name='logout'),
]
