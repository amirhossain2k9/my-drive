from django.apps import AppConfig


class StorageConfig(AppConfig):
    name = 'packages.storage'
