import os
import time
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver

from storage.models import Folder, File
from storage.signals import create_folder_path
from django.db.models.signals import post_save


class RegistrationTests(StaticLiveServerTestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_user_registration(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/register/'))

        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('tester4')

        username_input = self.selenium.find_element_by_name("email")
        username_input.send_keys('tester4@test.com')

        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('test1234')

        username_input = self.selenium.find_element_by_name("password2")
        username_input.send_keys('test1234')
        time.sleep(1)
        self.selenium.find_element_by_id('signup_button').click()
        time.sleep(3)
        self.selenium.find_element_by_id('create_folder')


class LoginTests(StaticLiveServerTestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_login(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('tester1')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('test1234')
        self.selenium.find_element_by_id('login_button').click()
        time.sleep(3)
        self.selenium.find_element_by_id('create_folder')


class LogOutTests(StaticLiveServerTestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_logout(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('tester1')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('test1234')
        self.selenium.find_element_by_id('login_button').click()
        time.sleep(3)
        self.selenium.find_element_by_id('logout_button')


class FolderCreationTests(StaticLiveServerTestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_folder_creation(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('tester1')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('test1234')
        self.selenium.find_element_by_id('login_button').click()

        self.selenium.execute_script('document.getElementById("create_folder").click()')
        time.sleep(3)

        folder_name = self.selenium.find_element_by_name('name')
        folder_name.send_keys('test_folder')
        time.sleep(1)
        create_folder_button = self.selenium.find_element_by_xpath('//*[@id="folder_creation_form"]/div[2]/div[2]/button').click()
        time.sleep(3)


class FileUploadingTests(StaticLiveServerTestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_file_uploading(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('tester1')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('test1234')
        self.selenium.find_element_by_id('login_button').click()

        file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'sample_test_file.txt')

        file_choice_field = self.selenium.find_element_by_name("file")
        file_choice_field.send_keys(file)
        time.sleep(2)
        self.selenium.execute_script('document.getElementById("upload_button").click()')
        time.sleep(3)


class FolderVisitingTests(StaticLiveServerTestCase):
    fixtures = ['user.json', 'storage']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_folder_visit(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('tester1')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('test1234')
        self.selenium.find_element_by_id('login_button').click()

        time.sleep(4)

        folder = self.selenium.find_element_by_xpath('//*[@id="folder_list_data"]/div[1]/a/div')
        folder.click()
        time.sleep(2)

    def test_back_to_parent_dir(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('tester1')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('test1234')
        self.selenium.find_element_by_id('login_button').click()

        time.sleep(4)

        folder = self.selenium.find_element_by_xpath('//*[@id="folder_list_data"]/div[1]/a/div')
        folder.click()
        time.sleep(2)
        self.selenium.find_element_by_id('parent_folder').click()
        time.sleep(4)


class FileSortingTests(StaticLiveServerTestCase):
    fixtures = ['user.json', 'storage']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_folder_visit(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('tester1')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('test1234')
        self.selenium.find_element_by_id('login_button').click()

        time.sleep(4)

        self.selenium.find_element_by_id('sort_desc_name').click()
        time.sleep(2)

        self.selenium.find_element_by_id('sort_asc_name').click()
        time.sleep(2)

        self.selenium.find_element_by_id('sort_desc_uploaded_at').click()
        time.sleep(2)

        self.selenium.find_element_by_id('sort_asc_uploaded_at').click()
        time.sleep(2)


