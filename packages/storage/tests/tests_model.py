import os
from django.test import TestCase
from django.contrib.auth.models import User
from storage.models import Folder, File

from storage.signals import create_folder_path
from django.db.models.signals import post_save



class FolderModelTest(TestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.file_path = os.path.dirname(os.path.abspath(__file__))
        cls.user = User.objects.get(username='tester1')
        cls.folder = Folder.objects.create(user=cls.user, name='test_folder')

    def test_folder_creation(self):
        new_folder = Folder.objects.create(user=self.user, name='new_folder')

        self.assertTrue(isinstance(new_folder, Folder))
        self.assertTrue(getattr(new_folder, 'user'))
        self.assertTrue(getattr(new_folder, 'name'))
        self.assertTrue(getattr(new_folder, 'created_at'))
        self.assertTrue(getattr(new_folder, 'parent') is None)

    def test_folder_creation_under_parent_folder(self):
        new_folder = Folder.objects.create(user=self.user, name='new_folder2', parent=self.folder)

        self.assertTrue(isinstance(new_folder, Folder))
        self.assertEqual(new_folder.parent, self.folder)


class FileModelTest(TestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.file_path = os.path.dirname(os.path.abspath(__file__))
        cls.user = User.objects.get(username='tester1')
        cls.folder = Folder.objects.create(user=cls.user, name='test_folder')

    def test_file_instance_creation(self):
        file = os.path.join(self.file_path, 'sample_test_file.txt')
        file_instance = File.objects.create(user=self.user, file=file, folder=self.folder)

        self.assertTrue(isinstance(file_instance, File))
        self.assertEqual(file_instance.file, file)
