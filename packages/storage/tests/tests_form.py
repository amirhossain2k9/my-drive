import os
from django.test import TestCase
from storage.forms import FileForm
from django.contrib.auth.models import User
from storage.models import Folder, File
from django.core.files.uploadedfile import SimpleUploadedFile

from storage.signals import create_folder_path
from django.db.models.signals import post_save


class FileFormTest(TestCase):
    fixtures = ['user.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        post_save.disconnect(create_folder_path, sender=Folder)
        cls.file_path = os.path.dirname(os.path.abspath(__file__))
        cls.user = User.objects.get(username='tester1')
        cls.folder = Folder.objects.create(user=cls.user, name='test_folder')

    def test_with_file_upload(self):

        post_data = {
            'user': self.user.id,
            'folder': self.folder.id,
        }

        with open(os.path.join(self.file_path, 'sample_test_file.txt'), 'rb') as upload_file:

            file_data = {
                'file': SimpleUploadedFile(upload_file.name, upload_file.read())
            }

            form = FileForm(post_data, file_data)
            self.assertTrue(form.is_valid())

    def test_without_file_upload(self):

        post_data = {
            'user': self.user.id,
            'folder': self.folder.id,
        }

        with open(os.path.join(self.file_path, 'sample_test_file.txt'), 'rb') as upload_file:

            file_data = {
                'file': ''
            }

            form = FileForm(post_data, file_data)
            self.assertFalse(form.is_valid())
