from django.contrib import admin

from storage.models import File, Folder

@admin.register(Folder)
class FolderAdmin(admin.ModelAdmin):
    list_display = ['user', 'name', 'created_at', 'parent' ]

@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    list_display = ['folder', 'file']
