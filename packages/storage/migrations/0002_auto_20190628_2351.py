# Generated by Django 2.2.2 on 2019-06-28 23:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='folder',
            old_name='uploaded_at',
            new_name='created_at',
        ),
    ]
