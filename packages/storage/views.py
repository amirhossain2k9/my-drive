from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from storage.models import File, Folder
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.http.response import JsonResponse

from django.core.exceptions import ObjectDoesNotExist

from storage.forms import FileForm


@login_required
def home(request):
    """
    Home page for authenticated user. All the folders created by this user
    and files uploaded by this user will be shown here. From Login  page
    and register page, authenticated user will be redirect here. User can
    sort his/her uploaded files here.
    :param request:
    :return:
    """
    order_by = request.GET.get('order_by', 'id')
    user = User.objects.get(id=int(request.user.id))

    sub_folders = Folder.objects.filter(user=user, parent=None).all()
    files = File.objects.filter(user=user, folder=None).order_by(order_by)

    return render(request, 'storage/home.html',
                  {
                      'request': request,
                      'sub_folders': sub_folders,
                      'files': files,
                      'parent_folder': None,
                      'current_folder': None,
                  })

@login_required
def create_folder(request):
    """
    This function creates new folder based on currently the user is on aka parent folder. But, for folders
    created in home directory (root), have no parent directory, all are mounted to the virtual '/' or
     root directory.
    :param request:
    :return:
    """
    if request.method == 'POST':
        parent_id = request.POST.get('parent', None)
        folder_name = request.POST.get('name')
        parent_folder = Folder.objects.get(user=request.user, id=int(parent_id)) if parent_id else None
        new_folder = Folder.objects.create(user=request.user, name=folder_name, parent=parent_folder)

        new_folder.save()

        after_folder_creation_template = render_to_string('storage/after_folder_creation_template.html',
                                                          {
                                                              'folder': new_folder
                                                          })

        return JsonResponse({
            'status': 'success',
            'after_folder_creation_template': after_folder_creation_template
        })
    return JsonResponse({
        'status': 'fail',
        'after_folder_creation_template': ''
    })

@login_required
def upload_file(request):
    """
    File uploading is processed by here. This function is triggered on a
    ajax request from the client.
    :param request:
    :return:
    """
    if request.is_ajax() and request.method == 'POST' and request.FILES:
        form = FileForm(request.POST, request.FILES)
        if form.is_valid():
            new_file = form.save()
            after_file_upload_template = render_to_string('storage/after_file_upload_template.html',
                                                          {'file': new_file})

            return JsonResponse({
                'status': 'success',
                'after_file_upload_template': after_file_upload_template
            })
    return JsonResponse({
            'status': 'fail',
        })

@login_required
def visit_folder(request, folder_id):
    """
    Each folder is visited with this function. It takes an argument of folder ID which
    was created by that logged in user. User can sort his/her uploaded files here.
    :param request:
    :param folder_id:
    :return:
    """
    order_by = request.GET.get('order_by', 'id')
    user = User.objects.get(id=int(request.user.id))
    sub_folders = []
    files = []
    parent_folder = None

    try:
        current_folder = Folder.objects.get(user=user, id=int(folder_id))
        sub_folders = Folder.objects.filter(user=user, parent=current_folder)
        files = File.objects.filter(user=user, folder=current_folder).order_by(order_by)
    except ObjectDoesNotExist as e:
        return redirect('/')
    else:
        parent_folder = current_folder.parent
    return render(request, 'storage/home.html',
                  {
                      'request': request,
                      'sub_folders': sub_folders,
                      'files': files,
                      'parent_folder': parent_folder,
                      'current_folder': current_folder,
                  })



