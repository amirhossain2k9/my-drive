import os
from django.conf import settings


def create_folder_path(sender, instance, created, *args, **kwargs):
    """
    This signal functions triggered after creation of a folder. It creates a physical
    directory under the parent directory in the 'MEDIA_ROOT' directory. If there is
    no parent directory for 'Folder' instance', the physical folder is created under
    the username directory. And the username directory is created too for that
    particular user when he/she first creates a folder.
    :param sender: Folder model
    :param instance: newly created Folder instance
    :param created: boolean value whether Folder instance created or not
    :param args:
    :param kwargs:
    :return:
    """
    if created:
        user_folder_name = os.path.join(settings.MEDIA_ROOT, instance.user.username)

        if not os.path.exists(user_folder_name):
            os.mkdir(user_folder_name)


        current_folder_path = os.path.join(user_folder_name, instance.name) if not instance.parent else os.path.join(instance.parent.folder_path, instance.name)
        if not os.path.exists(current_folder_path):
            os.makedirs(current_folder_path, exist_ok=True)
        instance.folder_path = current_folder_path
        instance.save()

