from django import forms
from storage.models import File


class FileForm(forms.ModelForm):
    """
    This model form of 'File' model is used to validate and upload
    file on a particular 'Folder'.
    """
    class Meta:
        model = File
        exclude = ['uploaded_at']