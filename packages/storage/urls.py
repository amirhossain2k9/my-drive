from django.urls import path, re_path
from storage import views

app_name = "storage"

urlpatterns = [
    re_path(r'^$', views.home, name='home'),
    re_path(r'^folder/(?P<folder_id>\d+)/$', views.visit_folder, name='visit_folder'),
    re_path(r'^create_folder/$', views.create_folder, name='create_folder'),
    re_path(r'^upload_file/$', views.upload_file, name='upload_file'),

]
