MY DRIVE
==========
'MY DRIVE' is a web based file storing system (Minimal version of Google drive) where everyone will be able to see and
upload files.

It has following properties:

    *  User registration and authentication
    *  User will be able to create and show folders(folder could be nested)
    *  Upload files under particular folder and display
    *  Sort files with their name
    *  Sort files with their uploaded times


System Requirements
====================
    > Python3.7
    > geckodriver
    > firefox browser


Installation and Configuration
===============================
    *  clone this repo to local: git clone https://amir_hossain2k9@bitbucket.org/amir_hossain2k9/my-drive.git
    *  go to project root directory: cd my-drive
    *  create a virtual environment: virtualenv -p python3.7 env
    *  activate virtual environment: source env/bin/activate
    *  install project required packages: pip install -r requirements.txt
    *  make database migrations: python manage.py migrate
    *  run web server: python manage.py runserver
    *  visit site at: 127.0.0.1:8000


Running Tests
==============
    > activate virtual environment
    > python manage.py test accounts storage


Active Issues
===============
    > visit: https://bitbucket.org/amir_hossain2k9/my-drive/issues?status=new&status=open


Further Documentations
======================
Please Visit 'doc' folder under project directory.